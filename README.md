# Autolib - Analysis

### By: Andrew Wairegi

## Description
To analyse Autolibs structure in Paris. By analysing different details
of their car hiring services, using their data. This will allow us to
know and understand how Paris's Autolib service is working in Paris.
As well as give them information on how to improve their service.

[Open notebook]

## Setup/installation instructions
1. Find a local folder on your computer
2. Setup the folder as an empty repository (git init)
3. Clone this remote repository into the local repository (git clone)
4. Upload the collaboratory notebook to google drive
5. Open it
6. Upload the dataset file to the file section of the google collab
7. Run the notebook

## Known Bugs
There are no known issues

## Technologies Used
1. Python - programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data analysis Package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.



[Open notebook]: https://gitlab.com/d7713/data-analysis/Analysis-Autolib_company/-/blob/main/Autolib_Analysis.ipynb
